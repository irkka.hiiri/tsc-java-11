package ru.tsc.ichaplygina.taskmanager.repository;

import ru.tsc.ichaplygina.taskmanager.api.ITaskRepository;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private final List<Task> list = new ArrayList<>();

    @Override
    public List<Task> findAll() {
        return this.list;
    }

    @Override
    public void add(final Task task) {
        this.list.add(task);
    }

    @Override
    public void remove(final Task task) {
        this.list.remove(task);
    }

    @Override
    public void clear() {
        this.list.clear();
    }

    @Override
    public Task findById(final String id) {
        for (Task task : this.list) {
            if (id.equals(task.getId())) return task;
        }
        return null;
    }

    @Override
    public Task findByName(final String name) {
        for (Task task : this.list) {
            if (name.equals(task.getName())) return task;
        }
        return null;
    }

    @Override
    public Task findByIndex(final int index) {
        return this.list.get(index);
    }

    @Override
    public Task removeById(final String id) {
        final Task task = findById(id);
        if (task == null) return null;
        this.remove(task);
        return task;
    }

    @Override
    public Task removeByIndex(final int index) {
        return this.list.remove(index);
    }

    @Override
    public Task removeByName(final String name) {
        final Task task = findByName(name);
        if (task == null) return null;
        this.remove(task);
        return task;
    }

}
