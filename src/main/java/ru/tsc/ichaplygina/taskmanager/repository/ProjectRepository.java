package ru.tsc.ichaplygina.taskmanager.repository;

import ru.tsc.ichaplygina.taskmanager.api.IProjectRepository;
import ru.tsc.ichaplygina.taskmanager.model.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private final List<Project> list = new ArrayList<>();

    @Override
    public List<Project> findAll() {
        return this.list;
    }

    @Override
    public void add(final Project project) {
        this.list.add(project);
    }

    @Override
    public void remove(final Project project) {
        this.list.remove(project);
    }

    @Override
    public void clear() {
        this.list.clear();
    }

    @Override
    public Project findById(final String id) {
        for (Project project : this.list) {
            if (id.equals(project.getId())) return project;
        }
        return null;
    }

    @Override
    public Project findByName(final String name) {
        for (Project project : this.list) {
            if (name.equals(project.getName())) return project;
        }
        return null;
    }

    @Override
    public Project findByIndex(final int index) {
        return this.list.get(index);
    }

    @Override
    public Project removeById(final String id) {
        final Project project = findById(id);
        if (project == null) return null;
        this.remove(project);
        return project;
    }

    @Override
    public Project removeByIndex(final int index) {
        return this.list.remove(index);
    }

    @Override
    public Project removeByName(final String name) {
        final Project project = findByName(name);
        if (project == null) return null;
        this.remove(project);
        return project;
    }

}
