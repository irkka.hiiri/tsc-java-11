package ru.tsc.ichaplygina.taskmanager.controller;

import ru.tsc.ichaplygina.taskmanager.api.IProjectController;
import ru.tsc.ichaplygina.taskmanager.api.IProjectService;
import ru.tsc.ichaplygina.taskmanager.model.Project;
import ru.tsc.ichaplygina.taskmanager.util.TerminalUtil;

public class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(final IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void showList() {
        int index = 1;
        for (Project project : projectService.findAll()) {
            System.out.println(index + ". " + project);
            index++;
        }
        if (index == 1) System.out.println("No projects yet. Type <create project> to add a project.");
        System.out.println();
    }

    @Override
    public void clear() {
        int count = projectService.findAll().size();
        System.out.println("Deleting all projects...");
        projectService.clear();
        System.out.println(count + " projects deleted");
        System.out.println();
    }

    @Override
    public void create() {
        System.out.println("Creating a new project.");
        final String name = TerminalUtil.readLine("Project name: ");
        final String description = TerminalUtil.readLine("Project description: ");
        if (projectService.add(name, description) == null) System.out.println("ERROR CREATING PROJECT!");
        else System.out.println("Done. Type <list projects> to view all projects.");
        System.out.println();
    }

    public void showProject(final Project project) {
        if (project == null) System.out.println("Project not found");
        else System.out.println(project);
        System.out.println();
    }

    @Override
    public void showById() {
        final String id = TerminalUtil.readLine("Enter id: ");
        final Project project = this.projectService.findById(id);
        showProject(project);
    }

    @Override
    public void showByIndex() {
        final int index = TerminalUtil.readNumber("Enter index: ");
        final Project project = this.projectService.findByIndex(index - 1);
        showProject(project);
    }

    @Override
    public void showByName() {
        final String name = TerminalUtil.readLine("Enter name: ");
        final Project project = this.projectService.findByName(name);
        showProject(project);
    }

    @Override
    public void updateById() {
        final String id = TerminalUtil.readLine("Enter id: ");
        final Project project = this.projectService.findById(id);
        if (project == null) System.out.println("Project not found");
        else {
            final String name = TerminalUtil.readLine("Project name: ");
            final String description = TerminalUtil.readLine("Project description: ");
            if (this.projectService.updateById(id, name, description) == null)
                System.out.println("ERROR UPDATING PROJECT!");
            else System.out.println("Project updated.");
        }
        System.out.println();
    }

    @Override
    public void updateByIndex() {
        final int index = TerminalUtil.readNumber("Enter index: ");
        final Project project = this.projectService.findByIndex(index - 1);
        if (project == null) System.out.println("Project not found");
        else {
            final String name = TerminalUtil.readLine("Project name: ");
            final String description = TerminalUtil.readLine("Project description: ");
            if (this.projectService.updateByIndex(index - 1, name, description) == null)
                System.out.println("ERROR UPDATING PROJECT!");
            else System.out.println("Project updated.");
        }
        System.out.println();
    }

    @Override
    public void removeById() {
        final String id = TerminalUtil.readLine("Enter id: ");
        final Project project = this.projectService.removeById(id);
        if (project == null) System.out.println("Project not found");
        else System.out.println("Project removed.");
        System.out.println();
    }

    @Override
    public void removeByName() {
        final String name = TerminalUtil.readLine("Enter name: ");
        final Project project = this.projectService.removeByName(name);
        if (project == null) System.out.println("Project not found");
        else System.out.println("Project removed");
        System.out.println();
    }

    @Override
    public void removeByIndex() {
        final int index = TerminalUtil.readNumber("Enter index: ");
        final Project project = this.projectService.removeByIndex(index - 1);
        if (project == null) System.out.println("Project not found");
        else System.out.println("Project removed");
        System.out.println();
    }

}
