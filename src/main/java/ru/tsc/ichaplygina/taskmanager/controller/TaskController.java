package ru.tsc.ichaplygina.taskmanager.controller;

import ru.tsc.ichaplygina.taskmanager.api.ITaskController;
import ru.tsc.ichaplygina.taskmanager.api.ITaskService;
import ru.tsc.ichaplygina.taskmanager.model.Task;
import ru.tsc.ichaplygina.taskmanager.util.TerminalUtil;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void showList() {
        int index = 1;
        for (Task task : taskService.findAll()) {
            System.out.println(index + ". " + task);
            index++;
        }
        if (index == 1) System.out.println("No tasks yet. Type <create task> to add a task.");
        System.out.println();
    }

    @Override
    public void clear() {
        int count = taskService.findAll().size();
        System.out.println("Deleting all tasks...");
        taskService.clear();
        System.out.println(count + " tasks deleted");
        System.out.println();
    }

    @Override
    public void create() {
        System.out.println("Creating a new task.");
        final String name = TerminalUtil.readLine("Task name: ");
        final String description = TerminalUtil.readLine("Task description: ");
        if (taskService.add(name, description) == null) System.out.println("ERROR CREATING TASK!");
        else System.out.println("Done. Type <list tasks> to view all tasks.");
        System.out.println();
    }

    public void showTask(final Task task) {
        if (task == null) System.out.println("Task not found");
        else System.out.println(task);
        System.out.println();
    }

    @Override
    public void showById() {
        final String id = TerminalUtil.readLine("Enter id: ");
        final Task task = this.taskService.findById(id);
        showTask(task);
    }

    @Override
    public void showByIndex() {
        final int index = TerminalUtil.readNumber("Enter index: ");
        final Task task = this.taskService.findByIndex(index - 1);
        showTask(task);
    }

    @Override
    public void showByName() {
        final String name = TerminalUtil.readLine("Enter name: ");
        final Task task = this.taskService.findByName(name);
        showTask(task);
    }

    @Override
    public void updateById() {
        final String id = TerminalUtil.readLine("Enter id: ");
        final Task task = this.taskService.findById(id);
        if (task == null) System.out.println("Task not found");
        else {
            final String name = TerminalUtil.readLine("Task name: ");
            final String description = TerminalUtil.readLine("Task description: ");
            if (this.taskService.updateById(id, name, description) == null) System.out.println("ERROR UPDATING TASK!");
            else System.out.println("Task updated.");
        }
        System.out.println();
    }

    @Override
    public void updateByIndex() {
        final int index = TerminalUtil.readNumber("Enter index: ");
        final Task task = this.taskService.findByIndex(index - 1);
        if (task == null) System.out.println("Task not found");
        else {
            final String name = TerminalUtil.readLine("Task name: ");
            final String description = TerminalUtil.readLine("Task description: ");
            if (this.taskService.updateByIndex(index - 1, name, description) == null) System.out.println("ERROR UPDATING TASK!");
            else System.out.println("Task updated.");
        }
        System.out.println();
    }

    @Override
    public void removeById() {
        final String id = TerminalUtil.readLine("Enter id: ");
        final Task task = this.taskService.removeById(id);
        if (task == null) System.out.println("Task not found");
        else System.out.println("Task removed.");
        System.out.println();
    }

    @Override
    public void removeByName() {
        final String name = TerminalUtil.readLine("Enter name: ");
        final Task task = this.taskService.removeByName(name);
        if (task == null) System.out.println("Task not found");
        else System.out.println("Task removed");
        System.out.println();
    }

    @Override
    public void removeByIndex() {
        final int index = TerminalUtil.readNumber("Enter index: ");
        final Task task = this.taskService.removeByIndex(index - 1);
        if (task == null) System.out.println("Task not found");
        else System.out.println("Task removed");
        System.out.println();
    }

}
