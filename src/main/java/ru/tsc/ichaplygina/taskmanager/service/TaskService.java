package ru.tsc.ichaplygina.taskmanager.service;

import ru.tsc.ichaplygina.taskmanager.api.ITaskRepository;
import ru.tsc.ichaplygina.taskmanager.api.ITaskService;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.List;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    private boolean isEmpty(final String string) {
        return (string == null || string.isEmpty());
    }

    private boolean isInvalidIndex(final int index) {
        return (index < 0 || index > this.taskRepository.findAll().size() - 1);
    }

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findAll() {
        return this.taskRepository.findAll();
    }

    @Override
    public Task add(final String name, final String description) {
        if (isEmpty(name)) return null;
        final Task task = new Task(name, description);
        this.taskRepository.add(task);
        return task;
    }

    @Override
    public void clear() {
        this.taskRepository.clear();
    }

    @Override
    public Task findById(final String id) {
        if (isEmpty(id)) return null;
        return this.taskRepository.findById(id);
    }

    @Override
    public Task findByName(final String name) {
        if (isEmpty(name)) return null;
        return this.taskRepository.findByName(name);
    }

    @Override
    public Task findByIndex(final int index) {
        if (isInvalidIndex(index)) return null;
        return this.taskRepository.findByIndex(index);
    }

    @Override
    public void remove(final Task task) {
        if (task == null) return;
        this.taskRepository.remove(task);
    }

    @Override
    public Task removeById(final String id) {
        if (isEmpty(id)) return null;
        return this.taskRepository.removeById(id);
    }

    @Override
    public Task removeByIndex(final int index) {
        if (isInvalidIndex(index)) return null;
        return this.taskRepository.removeByIndex(index);
    }

    @Override
    public Task removeByName(final String name) {
        if (isEmpty(name)) return null;
        return this.taskRepository.removeByName(name);
    }

    @Override
    public Task updateByIndex(final int index, final String name, final String description) {
        if (isInvalidIndex(index) || isEmpty(name)) return null;
        final Task task = findByIndex(index);
        if (task == null) return null;
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateById(final String id, final String name, final String description) {
        if (isEmpty(id) || isEmpty(name)) return null;
        final Task task = findById(id);
        if (task == null) return null;
        task.setName(name);
        task.setDescription(description);
        return task;
    }

}
