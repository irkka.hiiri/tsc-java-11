package ru.tsc.ichaplygina.taskmanager.service;

import ru.tsc.ichaplygina.taskmanager.api.IProjectRepository;
import ru.tsc.ichaplygina.taskmanager.api.IProjectService;
import ru.tsc.ichaplygina.taskmanager.model.Project;

import java.util.List;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    private boolean isEmpty(final String string) {
        return (string == null || string.isEmpty());
    }

    private boolean isInvalidIndex(final int index) {
        return (index < 0 || index > this.projectRepository.findAll().size() - 1);
    }

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Project> findAll() {
        return this.projectRepository.findAll();
    }

    @Override
    public Project add(final String name, final String description) {
        if (isEmpty(name)) return null;
        final Project project = new Project(name, description);
        this.projectRepository.add(project);
        return project;
    }

    @Override
    public void clear() {
        this.projectRepository.clear();
    }

    @Override
    public Project findById(final String id) {
        if (isEmpty(id)) return null;
        return this.projectRepository.findById(id);
    }

    @Override
    public Project findByName(final String name) {
        if (isEmpty(name)) return null;
        return this.projectRepository.findByName(name);
    }

    @Override
    public Project findByIndex(final int index) {
        if (isInvalidIndex(index)) return null;
        return this.projectRepository.findByIndex(index);
    }

    @Override
    public void remove(final Project project) {
        if (project == null) return;
        this.projectRepository.remove(project);
    }

    @Override
    public Project removeById(final String id) {
        if (isEmpty(id)) return null;
        return this.projectRepository.removeById(id);
    }

    @Override
    public Project removeByIndex(final int index) {
        if (isInvalidIndex(index)) return null;
        return this.projectRepository.removeByIndex(index);
    }

    @Override
    public Project removeByName(final String name) {
        if (isEmpty(name)) return null;
        return this.projectRepository.removeByName(name);
    }

    @Override
    public Project updateByIndex(final int index, final String name, final String description) {
        if (isInvalidIndex(index) || isEmpty(name)) return null;
        final Project project = findByIndex(index);
        if (project == null) return null;
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateById(final String id, final String name, final String description) {
        if (isEmpty(id) || isEmpty(name)) return null;
        final Project project = findById(id);
        if (project == null) return null;
        project.setName(name);
        project.setDescription(description);
        return project;
    }

}
