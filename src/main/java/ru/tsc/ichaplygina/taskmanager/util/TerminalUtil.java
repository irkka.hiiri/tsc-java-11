package ru.tsc.ichaplygina.taskmanager.util;

import java.util.Scanner;

public final class TerminalUtil {

    private TerminalUtil() {
    }

    private final static Scanner SCANNER = new Scanner(System.in);

    public static final String COMMAND_PROMPT = "> ";

    public static String readLine() {
        return SCANNER.nextLine().trim();
    }

    public static String readLine(String output) {
        System.out.print(output);
        return readLine();
    }

    public static int readNumber() {
        try {
            return Integer.parseInt(readLine());
        }
        catch (NumberFormatException e) {
            return -1;
        }
    }

    public static int readNumber(String output) {
        System.out.print(output);
        return readNumber();
    }

}
