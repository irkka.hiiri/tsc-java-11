package ru.tsc.ichaplygina.taskmanager.model;

public class Project extends AbstractBusinessEntity {

    public Project() {
    }

    public Project(String name) {
        super(name);
    }

    public Project(String name, String description) {
        super(name, description);
    }

}
