package ru.tsc.ichaplygina.taskmanager.model;

import ru.tsc.ichaplygina.taskmanager.util.NumberUtil;

public abstract class AbstractBusinessEntity {

    private static final String EMPTY = "";

    private static final String DELIMITER = " : ";

    private static final String PLACEHOLDER = "<empty>";

    private final String id = NumberUtil.generateId();

    private String name;

    private String description;

    public static String getEmpty() {
        return EMPTY;
    }

    public static String getDelimiter() {
        return DELIMITER;
    }

    public static String getPlaceholder() {
        return PLACEHOLDER;
    }

    public AbstractBusinessEntity() {
        this.name = EMPTY;
        this.description = EMPTY;
    }

    public AbstractBusinessEntity(final String name) {
        this.name = name != null ? name : EMPTY;
    }

    public AbstractBusinessEntity(final String name, final String description) {
        this.name = name != null ? name : EMPTY;
        this.description = description != null ? description : EMPTY;
    }

    public String getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return this.id
                + DELIMITER
                + (!this.name.equals(EMPTY) ? this.name : PLACEHOLDER)
                + DELIMITER
                + (!this.description.equals(EMPTY) ? this.description : PLACEHOLDER);
    }

}
