package ru.tsc.ichaplygina.taskmanager.constant;

public class CommandConst {

    public static final String CMD_VERSION = "version";

    public static final String CMD_ABOUT = "about";

    public static final String CMD_HELP = "help";

    public static final String CMD_INFO = "info";

    public static final String CMD_EXIT = "exit";

    public static final String CMD_LIST_COMMANDS = "list commands";

    public static final String CMD_LIST_ARGUMENTS = "list arguments";

    public static final String CMD_LIST_TASKS = "list tasks";

    public static final String CMD_CLEAR_TASKS = "clear tasks";

    public static final String CMD_CREATE_TASK = "create task";

    public static final String CMD_LIST_PROJECTS = "list projects";

    public static final String CMD_CLEAR_PROJECTS = "clear projects";

    public static final String CMD_CREATE_PROJECT = "create project";

    public static final String CMD_SHOW_PROJECT_BY_ID = "show project by id";

    public static final String CMD_SHOW_PROJECT_BY_INDEX = "show project by index";

    public static final String CMD_SHOW_PROJECT_BY_NAME = "show project by name";

    public static final String CMD_UPDATE_PROJECT_BY_INDEX = "update project by index";

    public static final String CMD_UPDATE_PROJECT_BY_ID = "update project by id";

    public static final String CMD_REMOVE_PROJECT_BY_INDEX = "remove project by index";

    public static final String CMD_REMOVE_PROJECT_BY_ID = "remove project by id";

    public static final String CMD_REMOVE_PROJECT_BY_NAME = "remove project by name";

    public static final String CMD_SHOW_TASK_BY_ID = "show task by id";

    public static final String CMD_SHOW_TASK_BY_INDEX = "show task by index";

    public static final String CMD_SHOW_TASK_BY_NAME = "show task by name";

    public static final String CMD_UPDATE_TASK_BY_INDEX = "update task by index";

    public static final String CMD_UPDATE_TASK_BY_ID = "update task by id";

    public static final String CMD_REMOVE_TASK_BY_INDEX = "remove task by index";

    public static final String CMD_REMOVE_TASK_BY_ID = "remove task by id";

    public static final String CMD_REMOVE_TASK_BY_NAME = "remove task by name";

}
