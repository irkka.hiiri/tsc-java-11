package ru.tsc.ichaplygina.taskmanager.api;

import ru.tsc.ichaplygina.taskmanager.model.Project;

import java.util.List;

public interface IProjectService {

    List<Project> findAll();

    Project findById(final String id);

    Project findByName(final String name);

    Project findByIndex(final int index);

    Project add(final String name, final String description);

    void remove(final Project project);

    Project removeById(final String id);

    Project removeByIndex(final int index);

    Project removeByName(final String name);

    Project updateByIndex(final int index, final String name, final String description);

    Project updateById(final String id, final String name, final String description);

    void clear();

}
