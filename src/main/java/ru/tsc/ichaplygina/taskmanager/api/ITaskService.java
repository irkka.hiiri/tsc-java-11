package ru.tsc.ichaplygina.taskmanager.api;

import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.List;

public interface ITaskService {

    List<Task> findAll();

    Task findById(final String id);

    Task findByName(final String name);

    Task findByIndex(final int index);

    Task add(final String name, final String description);

    void remove(final Task task);

    Task removeById(final String id);

    Task removeByIndex(final int index);

    Task removeByName(final String name);

    Task updateByIndex(final int index, final String name, final String description);

    Task updateById(final String id, final String name, final String description);

    void clear();

}
