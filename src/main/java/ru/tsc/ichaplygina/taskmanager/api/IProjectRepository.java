package ru.tsc.ichaplygina.taskmanager.api;

import ru.tsc.ichaplygina.taskmanager.model.Project;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.List;

public interface IProjectRepository {

    List<Project> findAll();

    Project findById(final String id);

    Project findByName(final String name);

    Project findByIndex(final int index);

    void add(final Project project);

    void remove(final Project project);

    Project removeById(final String id);

    Project removeByIndex(final int index);

    Project removeByName(final String name);

    void clear();

}
