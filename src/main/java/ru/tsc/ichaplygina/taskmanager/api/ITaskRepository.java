package ru.tsc.ichaplygina.taskmanager.api;

import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.List;

public interface ITaskRepository {

    List<Task> findAll();

    Task findById(final String id);

    Task findByName(final String name);

    Task findByIndex(final int index);

    void add(final Task task);

    void remove(final Task task);

    Task removeById(final String id);

    Task removeByIndex(final int index);

    Task removeByName(final String name);

    void clear();

}
